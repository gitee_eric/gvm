module gitee.com/voidint/gvm

go 1.16

require (
	github.com/Masterminds/semver v1.4.2
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/mholt/archiver v3.1.1+incompatible
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a
	github.com/urfave/cli v1.20.0
)

require (
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20190430165422-3e4dfb77656c // indirect
	github.com/nwaples/rardecode v1.0.0 // indirect
	github.com/pierrec/lz4 v2.0.5+incompatible // indirect
	github.com/smartystreets/assertions v0.0.0-20190401211740-f487f9de1cd3 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	golang.org/x/net v0.0.0-20190501004415-9ce7a6920f09 // indirect
)
